<?php


#######################################################################
#                          ACTIONS
#######################################################################

/**
 * Implementation of hook_rules_action_info().
 * @ingroup rules
 */
function activity_log_rules_action_info() {
  return array(

    'rules_action_activity_log' => array(
      'label' => t('Log an activity'),
      'eval input' => array(
        'uid_creator',

        'verb',
        'verb_you',

        'action',
        'action_multiple',
        'action_multiple_separator',

        'uid_target',
        'oid_target_type',
        'oid_target',
      ),

      'help' => 'Logs a social networking message',
      'module' => 'Activity Log',
    ),

  );
}

/**
 * Action drupal message configuration form.
 */
function rules_action_activity_log_form($settings, &$form) {

  $form['settings']['uid_creator'] = array(
    '#type' => 'textfield',
    '#size' => '15',
    '#maxsize' => '25',
    '#title' => t('User who created the activity'),
    '#default_value' => $settings['uid_creator'],
    '#description' => t('The user doing the activity'),
  );

  $form['settings']['verb'] = array(
    '#type' => 'textfield',
    '#size' => '35',
    '#maxsize' => '35',
    '#title' => t('The verb of the activity'),
    '#default_value' => $settings['verb'],
    '#description' => t('This will normally be placed after the user name'),
  );
  $form['settings']['verb_you'] = array(
    '#type' => 'textfield',
    '#size' => '35',
    '#maxsize' => '35',
    '#title' => t('The verb of the action when the "you" formed is being used'),
    '#default_value' => $settings['verb_you'],
    '#description' => t('This will normally be placed after the user name'),
  );


  $form['settings']['action'] = array(
    '#type' => 'textfield',
    '#size' => '255',
    '#maxsize' => '255',
    '#title' => t('The action string'),
    '#default_value' => $settings['action'],
    '#description' => t('This will be placed between the user id and the target'),
  );
  $form['settings']['action_multiple'] = array(
    '#type' => 'textfield',
    '#size' => '255',
    '#maxsize' => '255',
    '#title' => t('The action string, multiple format'),
    '#default_value' => $settings['action_multiple'],
    '#description' => t('Just like the action string, but used when there are multuple items'),
  );
  $form['settings']['action_multiple_separator'] = array(
    '#type' => 'textfield',
    '#size' => '25',
    '#maxsize' => '25',
    '#title' => t('Separator to use when there are multiple actions'),
    '#default_value' => $settings['action_multiple_separator'],
    '#description' => t('String to be used when multiple actions are put together'),
  );

  $form['settings']['uid_target'] = array(
    '#type' => 'textfield',
    '#size' => '15',
    '#maxsize' => '25',
    '#title' => t('User who was target of the action'),
    '#default_value' => $settings['uid_target'],
    '#description' => t('The user target of the activity'),
  );


  $options=array('node' => 'node','comment' => 'comment','user' => 'user');
  $form['settings']['oid_target_type'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#title' => t('The type of the object that received the action'),
    '#default_value' => $settings['oid_type'],
    '#description' => t('The type of the object target of the action'),
  );
  $form['settings']['oid_target'] = array(
    '#type' => 'textfield',
    '#size' => '15',
    '#maxsize' => '25',
    '#title' => t('Object that received the activity'),
    '#default_value' => $settings['oid_target'],
    '#description' => t('The object target of the activity'),
  );
}


/**
 * Action Implementation: Log an activity
 *
 * Note that this function is tricky!!!
 * 1) The same activity is not logged if it has happened within the last
 *    60 minutes. Fields checked: 
 *    'uid_creator', 'verb', 'action', 'uid_target', 'oid_target_type',
 *    'oid_target'
 *
 * 2) If an activity exists already after checking:
 *    'uid_creator', 'verb', 'action'
 *    AND the activity is less than 1 hour old, AND the activity has less than
 *    5 targets, then a new activity is NOT created. Instead, targets are
 *    added to the found action
 *    
 * This is to ensure that 1) The same activity isn't logged too many times if
 * something goes silly 2) Things are grouped properly
 *
 */
function rules_action_activity_log($settings){

  // Never ever log anonymous' actions. It would be a phenomenal amount
  // of clutter
  if ($settings['uid_creator'] == 0 ){
    return;
  }

  // Check if an identical message -- with identical everything -- was logged 
  // within the last hour
  $r=db_result(db_query("SELECT COUNT(*) FROM activity_log al LEFT JOIN activity_log_targets alt ON al.aid = alt.aid WHERE al.uid_creator=%d AND al.verb='%s' AND al.action='%s' AND alt.uid_target=%d AND alt.oid_target_type='%s' AND alt.oid_target=%d AND %d - al.activity_timestamp < %d ORDER BY activity_timestamp DESC", $settings['uid_creator'], $settings['verb'], $settings['action'], $settings['uid_target'], $settings['oid_target_type'], $settings['oid_target'], time(), variable_get('activity_log_grouping_seconds',3600)  ) );
  if($r){
    #drupal_set_message("NOPE: $r");
    return;
  }

  #drupal_set_message("HERE: $r");

  // Count how many times the user has done the same action, although on
  // different nids

  $r=db_fetch_object(db_query("SELECT COUNT(*) as count, al.aid FROM activity_log al LEFT JOIN activity_log_targets alt ON al.aid = alt.aid WHERE al.uid_creator=%d AND al.verb='%s' AND al.action='%s' AND alt.oid_target_type='%s' AND %d - al.activity_timestamp < %d GROUP BY alt.aid ORDER BY al.aid DESC", $settings['uid_creator'], $settings['verb'], $settings['action'], $settings['oid_target_type'], time() , variable_get('activity_log_grouping_seconds',3600)  ) );



  $count = $r->count;
  $aid = $r->aid;
  #drupal_set_message("READ COUNT: $count $aid ");


  // If there are too many entries already, then create a NEW "master" one
  // and then make sure that the "child" is added wit the right $aid

  #drupal_set_message("Comparing $count with ". variable_get('activity_log_grouping_how_many',5) );

  if($count >= variable_get('activity_log_grouping_how_many',5) || $count == 0 ){   
    db_query("INSERT INTO activity_log SET uid_creator=%d, verb='%s', verb_you='%s', action='%s', action_multiple='%s', action_multiple_separator='%s', activity_timestamp=%d", $settings['uid_creator'], $settings['verb'], $settings['verb_you'], $settings['action'], $settings['action_multiple'], $settings['action_multiple_separator'], time()  );
    $aid = db_last_insert_id('activity_log','aid'); 
  }

  db_query("INSERT INTO activity_log_targets SET aid = %d, uid_target=%d, oid_target_type='%s', oid_target=%d, target_timestamp=%d", $aid, $settings['uid_target'], $settings['oid_target_type'], $settings['oid_target'], time()  );

}


##################################################################
#                       TOKENS
##################################################################


/**
 * Implementation of hook_token_list().
 */
function activity_log_token_list($type = 'all') {

  if ($type == 'node') {
    $tokens['node']['title-link'] = t("The node's title with a link to it");
  }
  if ($type == 'user') {
    $tokens['user']['user-name-url'] = t("The user's name with a link to it");
  }

  return $tokens;
}


/**
 * Implementation of hook_token_values().
 */
function activity_log_token_values($type, $object = NULL, $options = array()) {
  $values = array();
  switch ($type) {

    case 'node':
      $values['title-link'] = l($object->title,'node/'.$object->nid);
      break;

    case 'user':
      $values['user-name-url'] = l($object->name,'user/'.$object->uid);
      break;

  }
  return $values;
}

