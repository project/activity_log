ACTIVITY LOG
============

Introduction
------------

This module is a _working_ experiment on how to create an Activity Log module
in Drupal writing as little code as possible.

The module is meant to work as glue between:

* A database structure with two simple tables
* The Views module, used to create blocks with the db structure
* The Rules module, used to intercept module's events and log things onto
  the activity log tables


This module is amazingly simple and yet amazingly powerful.
Advantages of using a glue module:

* No need for a new API. A lot of modules already interface themselves 
  with Rules. The ones that don't... should. Modules are far more likely to
  interface themselves with Rules than a custom Activity module

* The messages are very, very easy to customise. The Token module gives you 
  unthinkable power

* The integration with Views means that the module's abilities will grow along
  with Views'


The database structure
----------------------

Here is the db structure:

activity_log
------------

+---------------------------+----------------+-----+---------+----------------+
| Field                     | Type           | Key | Default | Extra          |
+---------------------------+----------------+-----+---------+----------------+
| aid                       |int(10) unsigned| PRI | NULL    | auto_increment | 
| uid_creator               |int(10) unsigned| MUL | 0       |                | 
| verb                      |varchar(35)     | MUL |         |                | 
| verb_you                  |varchar(35)     |     |         |                | 
| action                    |varchar(255)    | MUL |         |                | 
| action_multiple           |varchar(255)    |     |         |                | 
| action_multiple_separator |varchar(25)     |     |         |                | 
| activity_timestamp        |int(10) unsigned| MUL | 0       |                | 
+---------------------------+----------------+-----+---------+----------------+

activity_log_targets
--------------------

+------------------+------------------+------+-----+---------+----------------+
| Field            | Type             | Null | Key | Default | Extra          |
+------------------+------------------+------+-----+---------+----------------+
| atid             | int(10) unsigned | NO   | PRI | NULL    | auto_increment | 
| aid              | int(10) unsigned | NO   | MUL | 0       |                | 
| uid_target       | int(10) unsigned | NO   | MUL | 0       |                | 
| oid_target_type  | varchar(10)      | NO   | MUL |         |                | 
| oid_target       | int(10) unsigned | NO   | MUL | NULL    |                | 
| target_timestamp | int(10) unsigned | NO   | MUL | 0       |                | 
+------------------+------------------+------+-----+---------+----------------+

Here is a breakdown of the fields of "activity log":

* aid - Just an auto_increment id
* uid_creator - The user ID of the user creating the action
* verb, verb_you - It's the "verb" of the action. For example "has viewed".
  Note that there is a special verb if the user is watching him/herself (for
  example, "have viewed"

* action, action_multiple, action_multiple_separator - It's the action side of
  the story. Whereas "action" is the main one, there is an "action multiple"
  part which is applied of the action has more than one target. For example,
  "action" could be "the node" whereas "action_multiple" could be
  "the following nodes: ". Finally, "action_multiple_separator" is the
  separator used between each target.

* activity_timestamp - When the activity was added

Note that this gives a fair amount of flexibility in terms of how lines
can be composed. For example:

- merc has viewed the node "Example blog entry"
- you have viewed the following nodes: "Example blog entry", "Another", "Me"

While this may be considered English-centric, this will work with most
western languages. If it doesn't work for some, then users can change the
function theme_activity_log_line() so that it puts things "right".


Here is a breakdown of the fields of "activity log":

* atid - The activity target ID

* aid - The activity ID this target is referring to

* uid_target - The user which will be affected by the activity. This might not
  be defined at all

* oid_target_type - The type of object pointed by oid_target (the following
  record). For now, it can be "comment", "node", "user".

* oid_target - The object that got worked on

* target_timestamp - When the target was added


How things are added to the database
------------------------------------
Logging is done through the function rules_action_activity_log($settings);
This function is tricky.
While designing the whole thing, I figured out that I wanted this function
to do two things:

1) Make sure that the same activity is not repeated

2) Make sure that if the same activity is repeated on 5000 different
   objects, the system doesn't end up with ONE line in the activity_log
   table and 5000 lines in the activity_log_targets table. Which would be
   _really_ fun to render (not).


This means that it's up to this function to do things right: it won't log
the same event twice unless the second one is newer by X seconds (configurable),
and it won't group more than N targets together (configurable). If there are
more than N targets, a new activity_log line is created.

A note on the design
--------------------
Talking to other developers, some of them seem to think that the "action"
itself should be meta-information, rather than (tranlsatable) English.
However, doing something like that would:

* Create another level of complication in the module
* Make it harder to add custom messages

I mean, we could aggregate "verb" and "action" into a meta-description like
"NODE_VIEW". However, this "NODE_VIEW" would then need to be configured. And
would need to be a multi-lingual configuration. And it would need to take into
consideration who is watching the entry, to have "You" there (for example). And
it should take into consideration the fact that there could be one target or
multiple targets. There should be a way (hook?) to give Rules a list of
available "activity strings". And oh, did I mention that it should be
multilingual?
I think doing that would be a _huge_ overkill. Why not simply use the power of
t() and the theme_ hooks in Drupal to deal with pretty much anything that
could possibly come up?
A message is a message is a message. to me, the couple "verb" + "action" ARE
the message description. If you need something _very_ waky to happen (for
example, have a pair "verb" + "action" for which you want a completely
different rendering for), then you can use the theming functions. But it's
such a remote case, that it wouldn't be beneficial to change the whole
structure just for that. "Keep it simple stupid".
Of course, please feel free to prove me wrong. I might well be. However, I
have tortured myself with Facebook for moree than 2 hours just now, and
came to the conclusion that they must be following a structure similar to
what I have come up with for their messages.


Why a generic module won't cut it
---------------------------------
People asked me why I didn't just port the "system log" event module from
workflow_ng to to exactly this. The answer is "Because it's not enough". We
don't need to know what events are generated. We need to know who generated
what and who and what was affected by it.
Then, it would be very convenient if the "grouping" was done for us (good
luck writing the queries for presentation otherwise).

So, that's why. Of course, feel free to prove me wrong. If you manage to
generate the kind of output that Activity Log generates (with grouping of
maximum N items, avoiding repeated logging, etc.), then it means that this
module is useless.


Rules integration
-----------------
Rules integration seems to be complete.


Views integration
-----------------
Views integration is _incomplete_. I have basically put the basic table
descriptions out there. However, I am not sure how to get Views to do
what we need.

What I *DID* do, is to clarify VERY precisely what we need: I have created a
sample query function, 4 configurable blocks, and some theme functions. This
will allow us to see "what we should get Views to do" -- and it will also
allow people to use the module IF Views "can't do it" (which I doubt) or
IF a user wants to use this without Views (which is possible).

Everything is themable "the Drupal way". However, I am not sure if the theme
functions should be used by Views, for example.

Conclusion
----------
Well, that's it. IF I got it right, IF this solution/structure doesn't have
too many limitations, IF Views can be configured so that it works 100% OK,
then we have a glue module for actions.







