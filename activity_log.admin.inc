<?php

function activity_log_admin_settings() {

  $form['activity_log_no_duplicate_seconds'] = array(
      '#title' => t('Seconds before an identical activity can be logged again'),
      '#type' => 'textfield',
      '#size' => 8,
      '#default_value' => variable_get('activity_log_no_duplicate_seconds',3600)
    );
  $form['activity_log_grouping_seconds'] = array(
      '#title' => t('Maximum gap (in seconds) for the same activity to be grouped together'),
      '#type' => 'textfield',
      '#size' => 8,
      '#default_value' => variable_get('activity_log_grouping_seconds',3600)
    );
  $form['activity_log_grouping_how_many'] = array(
      '#title' => t('Maximum number of items that can be grouped'),
      '#type' => 'textfield',
      '#size' => 8,
      '#default_value' => variable_get('activity_log_grouping_how_many',5)
    );



  return system_settings_form($form);
}

?>
