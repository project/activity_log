<?php

/* 
http://www.garfieldtech.com/blog/drupal-handler-rfc
This is a general description of "handlers". Not specific to Views.

http://blue.live4all.co.il/~mooffie/tmp/views_illustrated/
This is a collection of notes I wrote. I have more stuff I'm planning to put there. But please don't distribute the link: it's a temporary address.

http://views.doc.logrus.com/
There's the inline documentation.

http://groups.drupal.org/views-developers
Ask for advice here as well.
*/

function activity_log_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'activity_log'),
      ),
    'handlers' => array(

/*
      'views_handler_field_XXXXXX' => array(
        'parent' => 'views_handler_field',
      ),

*/

    ), 
  );

}



function activity_log_views_data(){

  $data = array(

    'activity_log' => array(

      'table' => array(
        'group' => t('Activity log'),

        'base'  => array(
          'field' => 'aid',
          'title' => t('Activities'),
          'help'  => t("User activities on the site"),
        ),


      ),


      'aid' => array(
        'title' => t('Activity ID'),
        'help' => t('Activity ID'),
        'field' => array(
          'handler' => 'views_handler_field_numeric',
          'click sortable' => FALSE,
        ),
      ),

      'uid_creator' => array(
        'title' => t('Creating User'),
        'help' => t('The user who created the activity'),
        'relationship' => array(
          'base' => 'users',
          'field' => 'uid',
          'handler' => 'views_handler_relationship',
          'label' => t('User'),
        ),
      ),

      'uid_current' => array(
        'real field' => 'uid',
        'title' => t('Currently logged in user carried out the activity'),
        'help' => t('The user who did the activity is the currently logged in user'),
        'filter' => array(
          'handler' => 'views_handler_filter_user_current',
        ),
      ),


      'verb' => array(
        'title' => t('Verb'),
        'help' => t('The verb'),
        'field' => array(
          'handler' => 'views_handler_field',
        ),
      ),
      'verb_you' => array(
        'title' => t('Verb "you"'),
        'help' => t('The verb to be used if the user is the viewer'),
        'field' => array(
          'handler' => 'views_handler_field',
        ),
      ),


      'action' => array(
        'title' => t('Action'),
        'help' => t('The action'),
        'field' => array(
          'handler' => 'views_handler_field',
        ),
      ),
      'action_multiple' => array(
        'title' => t('Action Multiple'),
        'help' => t('The action string to be used if there are several targets'),
        'field' => array(
          'handler' => 'views_handler_field',
        ),
      ),
      'action_multiple_separator' => array(
        'title' => t('Action Multiple Separator'),
        'help' => t('The separator to be used between targets'),
        'field' => array(
          'handler' => 'views_handler_field',
        ),
      ),


      'activity_timestamp' => array(
        'title' => t('Activity timestamp'),
        'help' => t('When the activity happened'),
        'field' => array(
          'handler' => 'views_handler_field_date',
          'click sortable' => TRUE,
        ),
        'sort' => array(
          'handler' => 'views_handler_sort_date',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_date',
        ),
      ),



    ),



    'activity_log_targets' => array(

      'table' => array(
        'group' => t('Activity log targets'),

        'base'  => array(
          'field' => 'atid',
          'title' => t('Activities targets'),
          'help'  => t("Targets of an activity"),
        ),

        // FIXME: Not sure at all if this is the way to go
        'join' => array(
          'activity_log' => array(
            'left_field' => 'aid',
            'field' => 'aid',
          ),
        ),

      ),

      'aid' => array(
        'title' => t('Activity ID'),
        'help' => t('The activity of which this record is a target of'),
        'relationship' => array(
          'base' => 'activity_log',
          'field' => 'aid',
          'handler' => 'views_handler_relationship',
          'label' => t('Activity Log'),
        ),
      ),

      'uid_target' => array(
        'title' => t('Target User'),
        'help' => t('The user who was the target of the activity (if present)'),
        'relationship' => array(
          'base' => 'users',
          'field' => 'uid',
          'handler' => 'views_handler_relationship',
          'label' => t('User'),
        ),
      ),

      'uid_target_current' => array(
        'real field' => 'uid_target',
        'title' => t('Currently logged in user is the activity\'s recipient'),
        'help' => t('The user who was the target of the activity is the currently logged in user'),
        'filter' => array(
          'handler' => 'views_handler_filter_user_current',
        ),
      ),



      'oid_target_type' => array(
        'title' => t('The type of oid'),
        'help' => t('The type of oid, which can be a user, a comment or a node'),
        'field' => array(
          'handler' => 'views_handler_field',
        ),
      ),
      'oid_target' => array(
        'title' => t('ID of the target'),
        'help' => t('The id of a comment, a node or a user'),
        'field' => array(
          'handler' => 'views_handler_field_numeric',
        ),
      ),


      'target_timestamp' => array(
        'title' => t('Target timestamp'),
        'help' => t('When the target was added'),
        'field' => array(
          'handler' => 'views_handler_field_date',
          'click sortable' => TRUE,
        ),
        'sort' => array(
          'handler' => 'views_handler_sort_date',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_date',
        ),
      ),

    ),

  );

  return $data;

}


// THIS SHOULD LIVE IN THE friendlist_views module
// 
function activity_log_views_data_alter(&$data){

  // Add the right joins for the "friendlist_api" module
  if ( module_exists('friendlist_api') ){
    $data['activity_log']['table']['join']= array(
      'friendlist_statuses' => array(
        'left_field' => 'requestee_id',
        'field' => 'uid',
      )
    );
  }

}


